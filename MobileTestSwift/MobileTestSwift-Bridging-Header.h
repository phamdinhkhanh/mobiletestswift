//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//
#define MR_SHORTHAND

#import "AFNetworking/AFNetworking.h"
#import "CoreData+MagicalRecord.h"
#import <Mantle/Mantle.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <HCSStarRatingView/HCSStarRatingView.h>
#import <SpeechKit/SpeechKit.h>

