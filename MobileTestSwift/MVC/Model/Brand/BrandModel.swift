//
//  BrandModel.swift
//  MobileTestSwift
//
//  Created by DinhKhanh Pham on 3/27/16.
//  Copyright © 2016 DinhKhanh Pham. All rights reserved.
//

import Foundation

class brandModel: MTLModel, MTLJSONSerializing {
    var objectId : String! = ""
    var nameBrand : String! = ""
    var descBrand : String! = ""
    
    static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return["objectId": "objectId",
            "nameBrand" : "name",
            "descBrand" : "description"
            ]
    }
}