//
//  ProductModel.swift
//  MobileTestSwift
//
//  Created by DinhKhanh Pham on 3/27/16.
//  Copyright © 2016 DinhKhanh Pham. All rights reserved.
//

import Foundation

class ProductModel : MTLModel,MTLJSONSerializing {
    var availabilityStatus : String! = ""
    var colour  : String! = ""
    var dateCreated : DateCreateModel?
    var desProduct : String! = ""
    var objectId : String! = ""
    var productName : String! = ""
    var brandId : BrandIDModel?
    var price : Double! = 0
    
    static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["availabilityStatus"  : "availabilityStatus",
                "brandId"             : "brandID",
                "colour"              : "colour",
                "dateCreated"         : "dateCreated",
                "descProduct"         : "description",
                "objectId"            : "objectId",
                "productName"         : "productName",
                "price"               : "price"]
    }
    
    class func brandIdJSONTransformer() -> NSValueTransformer {
        return MTLJSONAdapter.dictionaryTransformerWithModelClass(BrandIDModel.self)
    }
    
    class func dateCreatedJSONTransformer() -> NSValueTransformer {
        return MTLJSONAdapter.dictionaryTransformerWithModelClass(DateCreateModel.self)
    }
}

class DateCreateModel : MTLModel, MTLJSONSerializing{
    var type : String = ""
    var isoDate : String = ""
    static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["type"      : "__type",
                "isoDate"   : "iso"]
    }
}

class BrandIDModel : MTLModel, MTLJSONSerializing{
    var type : String! = ""
    var className : String! = ""
    var objectId : String! = ""
    static func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["type"      : "__type",
                "className" : "className",
                "objectId"  : "objectId"]
    }
}