//
//  MTSServiceHelper.swift
//  MobileTestSwift
//
//  Created by DinhKhanh Pham on 3/27/16.
//  Copyright © 2016 DinhKhanh Pham. All rights reserved.
//

import Foundation

class MTSServiceHelper : FetcherProtocol  {
    func getAllProductsWithOnCompleteBlock(block: MTSResultsBlock) {
        
    }
    
    func getAllBrandWithOnCompleteBlock(block:MTSResultsBlock){
        
    }
    
    func getAllReviewsWithProductIdArray(productArray:AnyObject, OnCompleteBlock block:MTSResultsBlock){
        
    }
    
    func getAllReviewWithProductId(productId:String, OnCompleteBlock block:MTSResultsBlock){
        
    }
    
    func getAllUsersWithOnCompleteBlock(block:MTSResultsBlock){
        
    }
    
    func getAllProductWithBrandName(brandName:String, OnCompleteBlock bloc:MTSResultsBlock){
        
    }
}