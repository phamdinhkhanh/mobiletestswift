//
//  MTSStorage.swift
//  MobileTestSwift
//
//  Created by DinhKhanh Pham on 3/27/16.
//  Copyright © 2016 DinhKhanh Pham. All rights reserved.
//

import Foundation

protocol Storeage{
    func storeProducts(productArray:AnyObject)
    func storeBrands(brandArray:AnyObject)
    func storeReviews(reviewsArray:AnyObject)
    func storeUsers(usersArray:AnyObject)
}