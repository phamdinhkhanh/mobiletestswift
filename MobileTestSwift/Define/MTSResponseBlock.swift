//
//  MTSResponseBlock.swift
//  MobileTestSwift
//
//  Created by DinhKhanh Pham on 3/27/16.
//  Copyright © 2016 DinhKhanh Pham. All rights reserved.
//

import Foundation

typealias MTSSuccessBlock = ()->Void
typealias MTSResultJsonBlock = (result:AnyObject?, error:ErrorType)->Void
typealias MTSResultsSuccessBlock = (result:AnyObject)->Void
typealias MTSResultsBlock = (result:AnyObject?, error:ErrorType)->Void
typealias MTSFailureBlock = (error:ErrorType)->Void
typealias MTSResponseBlock = (success:Bool?, error:ErrorType)->Void
typealias MTSResponseWithMessageBlock = (message:String?, success:Bool?, error:ErrorType)
typealias MTSObjectBlock = (obj:AnyObject, error:ErrorType);